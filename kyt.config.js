const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const merge = require('webpack-merge')

module.exports = {
  hasServer: false,
  modifyWebpackConfig(cfg, opts) {
    const newCfg = {
      resolve: {
        extensions: ['.ts', '.tsx'],
        modules: [
          path.resolve(__dirname, 'src'),
          'node_modules'
        ]
      },
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            exclude: [
              path.resolve(__dirname, 'node_modules')
            ],
            include: [
              path.resolve(__dirname, 'src')
            ],
            use: ['babel-loader', 'awesome-typescript-loader']
          }
        ]
      },
      plugins: [
        new HtmlWebpackPlugin({
          title: 'Neato Starter Kyt',
          template: 'src/index.ejs'
        })
      ]
    }

    return merge.smart(cfg, newCfg)
  }
}
